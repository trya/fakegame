package com.mastergames.games.fakegame;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.mastergames.entities.Game;
import com.mastergames.entities.Group;
import com.mastergames.entities.User;

/**
 * Classe d'accès aux données
 */
public class Dao {
	private EntityManagerFactory factory = null;
	
	/**
	 * Initialise la gestion de la persistance.
	 */
	public void init() {
		//factory = Persistence.createEntityManagerFactory("dosiBase");
		factory = Persistence.createEntityManagerFactory("tunnelBase");
	}

	/**
	 * Ferme la gestion de la persistance.
	 */
	public void close() {
		if (factory != null) {
			factory.close();
		}
	}

	public Player findPlayer(long id) {
		EntityManager em = null;
		Player p = null;
		
		try {
			em = factory.createEntityManager();
			p = em.find(Player.class, id);
			return p;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	
	public Player addOrUpdatePlayer(Player p) {
		EntityManager em = null;
		Player pNew = null;
		
		if (p == null)
			throw new IllegalArgumentException("Entité nulle");
		
		try {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			pNew = em.merge(p);
			em.getTransaction().commit();
			return pNew;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	
	public Player removePlayer(long id) {
		EntityManager em = null;
		Player p = null;
		
		if (id == 0)
			throw new IllegalArgumentException("Identifiant nul");
		
		try {
			em = factory.createEntityManager();
			p = em.find(Player.class, id);
			if (p != null) {
				em.getTransaction().begin();
				em.remove(p);
				em.getTransaction().commit();
				return p;
			} else
				return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	
	public void truncatePlayer() {
		EntityManager em = null;
		try {
			em = factory.createEntityManager();
			em.getTransaction().begin();
			// utilisation de l'EntityManager
			Query q = em.createNativeQuery("TRUNCATE TABLE TPlayer");
			q.executeUpdate();
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
	
}