package com.mastergames.games.fakegame;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import com.mastergames.entities.User;
import com.mastergames.platform.HashTools;

/**
 * Servlet implementation class Controller
 * 
 * Administration de la plateforme
 * (gestion des jeux, des utilisateurs et des groupes)
 */
@WebServlet()
public class Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Dao dao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
		dao = new Dao();
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		dao.init();
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		dao.close();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pageJsp = null;
		String root = request.getServletPath();
		String action = request.getPathInfo();
		String method = request.getMethod();
		System.out.println(root);
		System.out.println(action);
		System.out.println(method);

		if (root.equals("/play")) {
			pageJsp = doPlay(request);
			if (action != null && !action.equals("/")) {
				response.sendRedirect("");
				return;
			}
		} else if (root.equals("/login")) {
			pageJsp = doLogin(request);
		} else if (root.equals("/logout")) {
			pageJsp = doLogout(request);
		} else {
			throw new ServletException("no matching action");
		}

		System.out.println(pageJsp);
		request.getRequestDispatcher(pageJsp).forward(request, response);
	}

	private String doLogout(HttpServletRequest request) {
		String jspPage = "/login.jsp";

		request.getSession(true).setAttribute("logged", false);
		request.getSession(true).invalidate();

		// TODO: message

		return jspPage;
	}

	private String doPlay(HttpServletRequest request) {
		String jspPage = "/fakegame.jsp";
		String action = request.getPathInfo();
		Player p = null;
		Boolean isLogged = (Boolean) request.getSession(true).getAttribute("logged");
		boolean logged = false;

		if (isLogged == null) {
			logged = false;
		} else {
			logged = isLogged;
		}

		if (logged) {
			p = (Player) request.getSession(true).getAttribute("player");
			if (action != null) {
				if (action.equals("/up")) {
					p.setScore(p.getScore()+100);
					System.out.println("hellup");
					System.out.println("id = " + p.getId());
					// nouveau contexte de persistance
					p = dao.addOrUpdatePlayer(p);
					request.getSession(true).setAttribute("player", p);
				} else if (action.equals("/down")) {
					p.setScore(p.getScore()-100);
					System.out.println("helldown");
					System.out.println("id = " + p.getId());
					p = dao.addOrUpdatePlayer(p);
					request.getSession(true).setAttribute("player", p);
				} else {
					// TODO: message pour action indéfinie
				}
			}
		}

		return jspPage;
	}

	private String doLogin(HttpServletRequest request) throws UnsupportedEncodingException {
		String jspPage = null;
		Boolean isLogged = (Boolean) request.getSession(true).getAttribute("logged");
		boolean logged = false;
		long id = 0;

		if (isLogged == null) {
			logged = false;
		} else {
			logged = isLogged;
		}

		if (!logged) {
			String url = "http://localhost:8080/projet_jeu/login";
			String charset = java.nio.charset.StandardCharsets.UTF_8.name();
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			// TODO: passer le numéro de jeu ou l'url pour vérification d'authenticité (ou bien SSL)

			if (username == null || password == null
					|| username.equals("") || password.equals("")) {
				return "/login.jsp";
			}

			String query = String.format("username=%s&password=%s", 
					URLEncoder.encode(username, charset), 
					URLEncoder.encode(password, charset));

			URLConnection conn = null;
			try {
				conn = new URL(url).openConnection();
			} catch (MalformedURLException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			}
			conn.setDoOutput(true); // Triggers POST.
			conn.setRequestProperty("Accept-Charset", charset);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);

			try (OutputStream output = conn.getOutputStream()) {
				output.write(query.getBytes(charset));
			} catch (IOException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			}

			InputStream input = null;
			String response = null;
			try {
				input = conn.getInputStream();
				response = IOUtils.toString(input, charset);
				input.close();
			} catch (IOException e) {
				// TODO Bloc catch généré automatiquement
				e.printStackTrace();
			}

			String[] parameters = response.split("\\&");
			for (int i = 0; i < parameters.length; i++) {
				String[] fields = parameters[i].split("=");
				String name = URLDecoder.decode(fields[0], charset);
				String value = URLDecoder.decode(fields[1], charset);
				System.out.println(name + " = " + value);
				if (name.equals("logged")) {
					if (value.equals("true")) {
						logged = true;
						//System.out.println("logged set to true");
					} else {
						logged = false;
					}
				} else if (name.equals("id")) {
					id = Long.parseLong(value);
				} else if (name.equals("username")) {
					/* 
					 * vérification du nom d'utilisateur
					 * (pas sensible à la casse car la clause SQL WHERE ne l'est pas) 
					 */
					if (!value.equalsIgnoreCase(username)) {
						throw new IllegalStateException("utilisateur non correspondant");
					} else {
						username = value;
					}
				}
			}

			if (logged) {
				Player p = dao.findPlayer(id);
				if (p == null) {
					p = new Player();
					p.setId(id);
					p.setScore(0);
					dao.addOrUpdatePlayer(p);
				}
				request.getSession(true).setAttribute("player", p);
				request.getSession(true).setAttribute("logged", logged);
				request.getSession(true).setAttribute("username", username);
			}

		} else {
			// TODO: déjà connecté
		}

		if (logged) {
			jspPage = "/login_success.jsp";
			request.getSession(true).setAttribute("logged", true);
		} else {
			jspPage = "/login.jsp";
			request.getSession(true).setAttribute("logged", false);
		}

		return jspPage;
	}

}
