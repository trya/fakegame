<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Un faux jeu</title>
</head>
<body>
	<%@ include file="header.jsp"%>
	<div class="main" id="game">
		<h1>Bienvenue sur un jeu factice !</h1>
		<c:choose>
			<c:when test="${logged}">
				Score : ${player.score}
				<form action="/fakegame/play/up">
					<div>
						<input type="submit" value="Augmenter" />
					</div>
				</form>
				<form action="/fakegame/play/down">
					<div>
						<input type="submit" value="Diminuer" />
					</div>
				</form>
			</c:when>
			<c:otherwise>
				Veuillez vous connecter pour jouer.
			</c:otherwise>
		</c:choose>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>