package com.mastergames.games.fakegame;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PostUpdate;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

import com.mastergames.entities.User;

@Entity(name = "Player")

@Table(name = "TPlayer")

/**
 * Entité de joueur
 */
public class Player implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id()
	private long id;

	@Basic(optional = false)
	@Column(name = "score", nullable = false)
	private long score;
	
	@Version()
	private long version = 0;

	@Transient
	public static long updateCounter = 0;
	
	@PreUpdate
	public void beforeUpdate() {
		System.err.println("PreUpdate of " + this);
	}

	@PostUpdate
	public void afterUpdate() {
		System.err.println("PostUpdate of " + this);
		updateCounter++;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
	
}