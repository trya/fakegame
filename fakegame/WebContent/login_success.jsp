<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Connexion réussie</title>
</head>
<body>
	<%@ include file="header.jsp"%>
	<div class="main" id="login_success">
		<p>Vous êtes à présent connecté.</p>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>