<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Connexion</title>
</head>
<body>
	<%@ include file="header.jsp"%>
	<div class="main" id="login">
		<h2>Connexion</h2>
		<form method="post" action="login">
			<table id="login_table">
				<tr>
					<td>Nom d'utilisateur :</td>
					<td><input type="text" name="username" /></td>
				</tr>
				<tr>
					<td>Mot de passe :</td>
					<td><input type="password" name="password" /></td>
				</tr>
				<tr>
					<td></td>
					<td class="submit_button"><input type="submit"
						value="Connexion" /></td>
				</tr>
			</table>
		</form>
	</div>
	<%@ include file="footer.jsp"%>
</body>
</html>