<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="header">
	<table id="header_table">
		<tr>
			<td id="index_header"><a href="/fakegame">Index</a></td>
			<c:choose>
				<c:when test="${logged}">
					<td id="login_header">Bonjour ${username}
						(<a href="/fakegame/logout">Se d&eacute;connecter</a>)</td>
				</c:when>
				<c:otherwise>
					<td id="register_header"><a href="/projet_jeu/register?game_id=${game_id}">S'inscrire</a>
					<td id="login_header"><a href="/fakegame/login">Se connecter</a></td>
				</c:otherwise>
			</c:choose>
		</tr>
	</table>
	<!-- Cette page emp�che la formation des bonnes url relatives -->
</div>